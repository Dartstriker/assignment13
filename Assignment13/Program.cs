﻿using System.Diagnostics;
using Newtonsoft.Json;
using System.Diagnostics;


namespace Assignment13
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var f = F.Get();
			string result = String.Empty;
			var stopwatch = new Stopwatch();

			stopwatch.Start();
			for (int i = 0; i < 100000; i++)
			{
				result = CustomSerializer.Serialize(f);
			}
			stopwatch.Stop();

			var stopwatch2 = new Stopwatch();
			stopwatch2.Start();
			Console.Write(result);
			Console.WriteLine(stopwatch.Elapsed / 100000);
			stopwatch2.Stop();
			Console.WriteLine(stopwatch2.Elapsed);

			var stopwatch3 = new Stopwatch();
			stopwatch3.Start();
			for (int i = 0; i < 100000; i++)
			{
				result = JsonConvert.SerializeObject(f);
			}
			stopwatch3.Stop();
			Console.WriteLine(result);
			Console.WriteLine(stopwatch3.Elapsed / 100000);

			Console.ReadLine();
		}
	}
}
