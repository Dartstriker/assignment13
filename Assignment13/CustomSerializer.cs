﻿using System.Reflection;
using System.Text;

namespace Assignment13;

public class CustomSerializer
{
	public static string Serialize(object input)
	{
		var type = input.GetType();
		var stringBuilder = new StringBuilder();

		foreach (var field in type.GetFields(BindingFlags.Public | BindingFlags.Instance))
		{
			stringBuilder.Append(field.Name);
			stringBuilder.Append(" ; ");
			stringBuilder.Append(field.GetValue(input));
			stringBuilder.Append(Environment.NewLine);
		}

		return stringBuilder.ToString();
	}
}